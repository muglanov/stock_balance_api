"""Основные кофигурации"""
import os
basedir = os.path.abspath(os.path.dirname(__file__))


class Config:
    DATABASE_FILE_NAME = 'stock_balance.db'
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or 'sqlite:///' + os.path.join(basedir, DATABASE_FILE_NAME)
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    DEBUG = bool(os.environ.get('DEBUG_MODE')) or True
