# Dockerfile
FROM python:3.6

# Создание и установка рабочей дирректории
RUN mkdir /code
WORKDIR /code
# Установка зависимостей
COPY requirements.txt /code
RUN pip install -r requirements.txt
# Копирование проекта
COPY ./config.py /code
COPY ./run_app.py /code
COPY ./app/ /code/app
