# Складские остатки
#### Запуск проекта

Для запуска проекта необходимо развернуть контейнер.
 - docker-compose -f docker-compose.yml up -d
 
Проект запустится на локальном хосте 0.0.0.0:5000.

##### Запуск тестов

 - python -m unittest test_api.py 

#### Api:
##### Get:
 - /product - возвращает список всех продуктов
 - /product/<product_group> - возвращает список всех продуктов с соответствующей product_group группой продуктов
##### Post:
 - /product - создаёт новую запись product
 
 Принимает json вида {"name": str, "sku": str, "product_group": str, "balance": int}
##### Put:
 - /product - изменяет balance у product с product_id
 
Принимает json вида {"product_id": int, "balance": int} 
##### Delete:
 - /product удаляет обьект по product_id
 
 Принимает json вида {"product_id": int, "balance": int} 

