import os
from unittest import TestCase, main

from app import app, db
from config import Config


class TestFlaskApi(TestCase):

    @classmethod
    def setUpClass(cls):
        cls.web_api = app.test_client()

    def setUp(self):
        db.create_all()

    def tearDown(self):
        os.remove(os.path.join(os.path.dirname(os.getcwd()), Config.DATABASE_FILE_NAME))

    def test_rest_with_right_data(self):
        # Проверка добавления нового элемента
        data = {
            'name': 'name1',
            'sku': 'sku1',
            'product_group': 'group1',
            'balance': 1
        }
        response = self.web_api.post('/product', json=data)
        self.assertTrue(response.json['error'] is None)
        self.assertTrue(isinstance(response.json['data'], int))
        data['product_id'] = response.json['data']

        # Проверка получения списка всех элементов
        result_data = [data]
        response = self.web_api.get('/product')
        self.assertTrue(response.json['error'] is None)
        self.assertEqual(response.json['data'], result_data)

        # Проверка изменения остатка
        data['balance'] = 3
        response = self.web_api.put('/product', json=data)
        self.assertTrue(response.json['error'] is None)
        self.assertTrue(response.json['data'])
        response = self.web_api.get('/product')
        self.assertEqual(response.json['data'][0]['balance'], data['balance'])

        # Проверка получения списка элементов по группе продуктов
        data = {
            'name': 'name2',
            'sku': 'sku2',
            'product_group': 'group2',
            'balance': 2
        }
        response = self.web_api.post('/product', json=data)
        data['product_id'] = response.json['data']
        result_data = [data]
        response = self.web_api.get(f'/product/{data["product_group"]}')
        self.assertTrue(response.json['error'] is None)
        self.assertEqual(response.json['data'], result_data)

        # Проверка удаления элементов
        response = self.web_api.get('/product')
        self.assertTrue(response.json['data'])
        for product in response.json['data']:
            response = self.web_api.delete('/product', json=product)
            self.assertTrue(response.json['error'] is None)
            self.assertTrue(response.json['data'])
        response = self.web_api.get('/product')
        self.assertEqual(response.json['data'], [])

    def test_post_duplicate_data(self):
        # Проверка добавления двух одинаковых элементов
        data = {
            'name': 'name1',
            'sku': 'sku1',
            'product_group': 'group1',
            'balance': 1
        }
        self.web_api.post('/product', json=data)
        response = self.web_api.post('/product', json=data)
        self.assertFalse(response.json['data'])
        self.assertTrue(response.json['error'])

    def test_put_with_wrong_data(self):
        # Проверка изменения с неправильными данными без ключа balance
        data = {
            'name': 'name1',
            'sku': 'sku1',
            'product_group': 'group1',
            'balance': 1
        }
        response = self.web_api.post('/product', json=data)
        data['product_id'] = response.json['data']
        del data['balance']
        response = self.web_api.put('/product', json=data)
        self.assertFalse(response.json['data'])
        self.assertTrue(response.json['error'])

        # Проверка изменения с неправильным ключем
        data['product_id'] = 0
        response = self.web_api.put('/product', json=data)
        self.assertFalse(response.json['data'])
        self.assertTrue(response.json['error'])


if __name__ == '__main__':
    main()
