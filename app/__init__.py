"""Основной модуль приложения Flask-RESTFul"""
import sys

from flask import Flask
from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

from config import Config

# Инициализация основных модулей Flask
app = Flask(__name__)
app.config.from_object(Config)
db = SQLAlchemy(app)
migrate = Migrate(app, db)
api = Api(app)

# Роутинг
from app.product.resource import Product
api.add_resource(Product, '/product', '/product/<string:product_group>')


def run_app():
    """Старт приложения Flask"""
    db.create_all()
    host = sys.argv[1]
    port = int(sys.argv[2])
    app.run(debug=Config.DEBUG, host=host, port=port)
