"""Описание модели Product"""
from app import db


class Product(db.Model):
    __tablename__ = 'product'
    product_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(), nullable=False, unique=True)
    sku = db.Column(db.String(), nullable=False, unique=True)
    product_group = db.Column(db.String(), nullable=False)
    balance = db.Column(db.Integer)

    def to_json(self):
        return {
            "product_id": self.product_id,
            "name": self.name,
            "sku": self.sku,
            "product_group": self.product_group,
            "balance": self.balance
        }

