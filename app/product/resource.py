"""HTTP ресурс Product"""
from flask_restful import Resource, request

from app import db
from app.utils import wrap_response
from app.utils import NoObjectWithIdException
from .model import Product as ProductModel


class Product(Resource):

    @wrap_response
    def get(self, product_group=''):
        if product_group:
            products = ProductModel.query.filter_by(product_group=product_group)
        else:
            products = ProductModel.query.all()
        return [product.to_json() for product in products]

    @wrap_response
    def post(self):
        new_product = ProductModel(**request.json)
        db.session.add(new_product)
        db.session.commit()
        return new_product.product_id

    @wrap_response
    def put(self):
        product_id = request.json.get('product_id')
        product = ProductModel.query.get(product_id)
        if not product:
            raise NoObjectWithIdException(f"Missing object with id: {product_id}")
        product.balance = request.json['balance']
        db.session.commit()
        return True

    @wrap_response
    def delete(self):
        product_id = request.json.get('product_id')
        product = ProductModel.query.get(product_id)
        if not product:
            raise NoObjectWithIdException(f"Missing object with id: {product_id}")
        db.session.delete(product)
        db.session.commit()
        return True
