"""Модуль со вспомогательными функциями"""
from functools import wraps


def wrap_response(func):
    """Декоратор возвращаищи словарь вида:
    {
        'data':True - при успешном выполнении или список объектов.
               False - если во время выполнения произошли ошибки.
        'error': None если ошибок нет или строковую репрезентацию ошибки
    }
    """
    @wraps(func)
    def wrapper(*args, **kwargs):
        data = False
        error = None
        try:
            data = func(*args, **kwargs)
        except Exception as e:
            error = str(e)
        return {
            'data': data,
            'error': error
        }
    return wrapper


class NoObjectWithIdException(Exception):
    """Ошибка если отсутствует объект с переданным ключем"""
    pass
